var i =0;
var dp = function (str){
  $('#xyzzy').html(  i+' ' +str+'<br />'+ $('#xyzzy').html())
  i++;
};
var CustomRandom = function(nseed) {
	var seed;
	var origseed;
	var constant = Math.pow(2, 13)+1;
	var prime = 1987;
	//any prime number, needed for calculations, 1987 is my favorite:)
	var maximum = 1000;
	//maximum number needed for calculation the float precision of the numbers (10^n where n is number of digits after dot)
	if (nseed) {
		seed = nseed;
	}
	if (seed == null) {
		//before you will correct me in this comparison, read Andrea Giammarchi's text about coercion http://goo.gl/N4jCB
		seed = (new Date()).getTime();
 		//if there is no seed, use timestamp
 	}
 	//alert ('seed'+seed);
 	origseed = seed;
 	return {
 		next : function(min, max) {
                        var t=seed;
 			seed *= constant;
 			seed += prime;
 			dp( 'seed:'+seed+' maximum:'+maximum+' min:' +min+' max:' +max);
 			if(!isFinite(seed)){
                          seed = t;
                          while(seed>origseed){
                            seed=seed/(constant+prime);
                          }
                          dp('seed overflow'+seed);

                        }
 			return min && max ?
 				min+seed%maximum/maximum*(max-min) :
				seed%maximum/maximum;
 				// if 'min' and 'max' are not provided, return random number between 0 & 1
		},
		seed : function () {
			return origseed;
		}
	} // */
};



// Iterate over all the table cells, and pick a word for each
$(document).ready(function(){
	// Generate a list of available word-numbers
	wl = $('#wordlist li');
	var avail = new Array();
	var i = 0;	
	wl.each(function(){
		avail[i] = i;
		i++;
	});
	
	var seed = getUrlParameter('seed') || 0;
	var rng = CustomRandom(seed);

	// Shuffle the numbers
	wl.each(function(){
          var num1 = rng.next();
          var num2 = rng.next();
		s1 = Math.floor(num1*wl.length);
		s2 = Math.floor(num2*wl.length);
		if(s1 == 13 || s2 == 13){
                  dp( avail[s1] + ' ['+s1+','+s2+']'+wl.html()+' '+wl.length+num1+ num2);

                }
                else{
                  //dp(avail[s1] + ' no');
                }
		temp = avail[s1]; avail[s1] = avail[s2]; avail[s2] = temp;
	});

	// Fill the table cells with words in order of that list
	//start with the free space:
	$(wl[avail[12]]).text( 'freespace!');

	i = 0;
	$("td").each(function() {
		$(this).html( $(wl[avail[i]]).html());
		if (i==13){
			//$(this).html( $(wl[avail[i]]).html());
		}
		$(this).click(function(){
			if($(this).attr('cur-color')=="yellow"){
				$(this).attr('cur-color',"white");
			}				
			else{
				$(this).attr('cur-color',"yellow");
			}
			
			$(this).css("background-color", $(this).attr('cur-color'));
		});
		i++;
	});
        var initial_seed =rng.seed();
	$('#seedLink').attr('href', '?seed='+initial_seed);
        $('#seedLink').text($('#seedLink').text()+ ' ' +initial_seed);
});

function getUrlParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
}